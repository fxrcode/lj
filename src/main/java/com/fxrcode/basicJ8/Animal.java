package com.fxrcode.basicJ8;

public class Animal {
    public String breed;
    public int legs;

    public Animal() {}
    public Animal(String b, int l) {
        this.breed = b;
        this.legs = l;
    }

    @Override
    public String toString() {
        return "Animal{" +
                "breed='" + breed + '\'' +
                ", legs=" + legs +
                '}';
    }

    public void act() {
        System.out.println("Just live in the earth");
    }
}
