package com.fxrcode.basicJ8;

import java.util.ArrayList;
import java.util.List;

public class Repo {
    List<Animal> animals = new ArrayList<>();
    List<Dog> dogs = new ArrayList<>();

    public Repo() {
        animals.add(new Animal("Ghost", 2));
        animals.add(new Animal("Milky", 2));
        dogs.add(new Dog("Milky", 4, "Beagle", 5));
        dogs.add(new Dog("Milky", 4, "2-ha", 10));
    }

    public void print() {
        for (Animal a : animals) {
            System.out.println("Animal: " + a);
        }
        for (Dog d : dogs) {
            System.out.println("Dog: " + d);
        }
    }

    public List<Animal> list() {
        List<Animal> ret = new ArrayList<>();
        ret.addAll(animals);
        ret.addAll(dogs);
        return ret;
    }

    public static void main(String[] args) {
        Repo  r = new Repo();
//        r.print();
        List<Animal> list = r.list();
        for (Animal a : list) {
            System.out.println("All-in-1 list: " + a);
        }
    }
}
