package com.fxrcode.basicJ8;

public class Dog extends Animal {
    public String name;
    public int times;

    public Dog() {
        super();
    }

    public Dog(String name, int age) {
        this.name = name;
        this.times = times;
    }

    public Dog(String b, int l, String name, int t) {
        super(b, l);
        this.name = name;
        this.times = t;
    }

    public void play() {
        for (int i = 0; i < times; ++i) {
            System.out.println("Let me jump: " + i + " times");
        }
    }

    @Override
    public String toString() {
        return "Dog{" +
                "name='" + name + '\'' +
                ", times=" + times +
                ", breed='" + breed + '\'' +
                ", legs=" + legs +
                '}';
    }

    @Override
    public void act() {
        System.out.println("Wang wang wang!!!");
    }
}
