package com.fxrcode.solutions;

import java.util.Arrays;

/**
 * Given a string s, find the longest palindromic substring in s. You may assume that the maximum length of s is 1000.
 *
 * Example 1:
 *
 * Input: "babad"
 * Output: "bab"
 * Note: "aba" is also a valid answer.
 * Example 2:
 *
 * Input: "cbbd"
 * Output: "bb"
 */
public class _005 {
    public static class Solution1 {
        public int ai = 0;
        public String as = null;

        private boolean isParlindome(String s, int b, int e) {
            int begin = b;
            int end  = e;
            while (s.charAt(begin) == s.charAt(end) && end-begin > 1) {
                begin++;
                end--;
            }
            if (s.charAt(begin) == s.charAt(end) && end-begin <= 1) {
//                System.out.println(s.substring(b, e+1));
                return true;
            } else {
                return false;
            }

        }

        public String longestPalindrome(String s) {
            if (s == null || s.length() < 1) {
                return s;
            }
            int lo = s.length();
            for (int i = 0; i < lo; ++i)  {
                for (int j = 0; j <= i; ++j) {
                    if (isParlindome(s, j, i) && ((i+1-j) > ai)) {
                        as = s.substring(j, i+1);
                        ai = i-j;
                    }
                }
            }
            return as;
        }
    }

    public static class Solution2 {
        private static String S;
        private static int LEN;
        private int[] dp;
        public String longestPalindrome(String s) {
            LEN = s.length();
            dp = new int[LEN];
            Arrays.fill(dp, -1);
            for (int i = 1; i < LEN; ++i) {
                if (dp[i-1] < 0) {
                    if (s.charAt(i) == s.charAt(i-1)) {
                        dp[i] = dp[i - 1] + 1;
                    } else if (i - (-dp[i-1]) - 1 >= 0 && s.charAt(i) == s.charAt(i - (-dp[i-1]) - 1)) {
                        dp[i] = -dp[i-1] + 2;
                    }
                } else {
                    if (i - dp[i-1] - 1 >= 0 && s.charAt(i) == s.charAt(i - dp[i-1] - 1)) {
                        dp[i] = dp[i-1] + 2;
                    }
                }
            }

            for (int i = 0; i < LEN; ++i) {
                dp[i] = Math.abs(dp[i]);
                System.out.println("[D]\t" + dp[i]);
            }
            int ai = 0;
            String as = s.substring(1);
            for (int i = 0; i < LEN; ++i) {
                if (dp[i] > ai) {
                    ai = dp[i];
                    as = s.substring(i - dp[i] + 1, i+1);
                }
            }
            return as;
        }
    }

    public static void main(String[] args) {
        System.out.println("Hello world!");
        String ex1 = "babab";  // "babad";
        String ex2 = "cbbd";
        String ex3 = "abcd";

        Solution1 s1 = new Solution1();
        System.out.println("Ans1 = " + s1.longestPalindrome(ex1));

        Solution2 s2 = new Solution2();
        System.out.println("Ans2 = " + s2.longestPalindrome(ex1));
    }
}
